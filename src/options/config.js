// 类型字段
const typeFieldList = [
    ["input", "input"],
    ["select", "select"],
    ["search", "search"],
    ["tree", "tree"],
    ["date", "date"],
    ["datetime", "datetime"],
    ["time", "time"],
    ["textarea", "textarea"],
    ["radio", "radio"],
    ["switch", "switch"],
    ["checkbox", "checkbox"],
    ["password", "password"],
];

// 下拉 单选 多选 数据
const whetherFieldList = [
    ["否", false],
    ["是", true],
];


// 下拉选择类型
const selectType = ["select", "radio", "switch", "tree", "checkbox"];

// 转换数值字段
const numField = [
    "searchMenuSpan",
    "labelWidth",
    "width",
    "minWidth",
    "menuWidth",
    "searchLabelWidth",
    "span",
    "searchOrder",
    "searchSpan"
];

// 不必要字段
const passAttribute = [
    "isRules",
    "rulesMax",
    "rulesMin",
    "hideType",
    "selectData",
    "menuLeft",
    "menu",
    "menuFormBtn",
    "searchMenu",
    "nativeData",
];

const defaultSelectData = [
    ['item1', '0'],
    ['item2', '1']
];

const defaultSelectDataStr = '["item1","0"]||["item2","1"]';

// 本地默认多选数据
const nativeData = `["是","0"]||["否","1"]

["正常","0"]||["暂停","1"]

["固定","0"]||["区间","1"]

["上架","0"]||["下架","1"]


证件类型
["营业执照","1"]||["身份证","2"]

["item1","0"]||["item2","1"]||

月份
["1月","1"]||["2月","2"]||["3月","3"]||["4月","4"]||["5月","5"]||["6月","6"]||["7月","7"]||["8月","8"]||["9月","9"]||["10月","10"]||["11月","11"]||["12月","12"]

周次
["星期一","1"]||["星期二","2"]||["星期三","3"]||["星期四","4"]||["星期五","5"]||["星期六","6"]||["星期日","7"]
`;



// 表单默认值
const formDefault = {
    span: "12",
    type: "input",
    search: false,
    isRules: false,
    hideType: "form", // form column all
    nativeData,
    // disabled: true, 
    // readonly: true, 
    // search: true
}



// 通用配置
const commonDefault = {
    addTitle: "新增文本",
    editTitle: "编辑文本",
    viewTitle: "查看文本",
    cancelBtnText: "退出",
    indexLabel: "序号",
    showIndex: true,
    index: true,
    border: true,
    selection: true,
    editBtn: true,
    viewBtn: true,
    addBtn: true,
    delBtn: false,
    tip: false,
    showHeade: true,
    searchMenuSpan: "24",
    align: "center",
    menuAlign: "center",
}
// 替换prop
const propDefault = {
    url: "",
    attributes: "",
    // url: "http://129.204.93.177:8069/blade-desk/v2/api-docs-ext",
    // attributes: "paths./notice/detail.get.parameters"
}



const slotStr = {
    menuLeft: `<template slot="menuLeft" slot-scope="scope" >
                <el-button type="primary" plain size="small" icon="el-icon-download">自定义按钮</el-button>
            </template>`,
    menu: `<template slot-scope="{ type, size }" slot="menu">
                <el-button icon="el-icon-check" :size="size" :type="type"
                >保存</el-button>
            </template>`,
    searchMenu: `<template slot="searchMenu" slot-scope="scope">
                <el-button size="small">自定义表单搜索按钮</el-button>
            </template>`,
    menuForm: `<template slot-scope="{ row, index, type }" slot="menuForm">
                    <el-button
                        type="primary"
                        icon="el-icon-check"
                        size="small"
                        @click="$refs.crud.rowUpdate()"
                        >自定义提交修改</el-button
                    >
                    <el-button
                        type="primary"
                        icon="el-icon-circle-plus-outline"
                        size="small"
                        v-if="type == 'add'"
                        @click="$refs.crud.rowSave()"
                        >保存</el-button
                    >
                    <el-button
                        type="primary"
                        icon="el-icon-circle-check"
                        size="small"
                        v-if="type == 'edit'"
                        @click="$refs.crud.rowUpdate()"
                        >修改</el-button
                    >
                    <el-button
                        icon="el-icon-circle-close"
                        size="small"
                        plain
                        @click="$refs.crud.closeDialog()"
                        >取消</el-button
                    >
                </template>`,
    childrenTableslot: `label: "",
                        prop: "table",
                        labelWidth: 0,
                        span: 24,
                        row: true,
                        formslot: true,`

}


export {
    typeFieldList,
    whetherFieldList,
    selectType,
    numField,
    passAttribute,
    defaultSelectData,
    defaultSelectDataStr,
    formDefault,
    commonDefault,
    propDefault,
    slotStr,
    nativeData
}