const numReg = /[^\d]/g;

function handleVerify(value, key) {
    this.$nextTick(() => {
        this.form[key] = value.toString().replace(numReg, "");
    });
}

function handleVerifySelectData(value) {
    const dataStr = `[${value.replace(/\|\|/g, ",")}]`;
    return JSON.parse(dataStr.replace(/\s/g, ''));
}


export { numReg, handleVerify, handleVerifySelectData }