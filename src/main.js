import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
Vue.use(Avue);


Vue.config.errorHandler = function (err, vm, info) {
  console.group('错误信息');
  console.log(err);
  console.groupEnd();
  console.group('Vue实例');
  console.log(vm);
  console.groupEnd();
  console.group('错误API');
  console.log(info);
  console.groupEnd();
}

new Vue({
  render: h => h(App),
}).$mount('#app')
